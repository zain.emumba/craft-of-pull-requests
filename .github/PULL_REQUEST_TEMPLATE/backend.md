## [API Change: Title of API Change]

**Description:**
[Provide details about the API changes, the motivation, and how they improve the project.]

**Related Documentation/Endpoints:**

- [Link to updated API documentation]
- [List new/changed endpoints]

**Testing Instructions:**

- [Include steps for using tools like Postman or Swagger]
- [Detail any new request/response formats]

**Code Changes:**

- [List major alterations in codebase]

**Checklist:**

- [ ] I have tested the changes.
- [ ] My code follows the project's standards.
- [ ] I have updated API documentation accordingly.

**Screenshots/API Responses:**
[Include screenshots of API responses, Postman collections, etc.]

**Additional Notes:**
[Include notes about dependencies, environment variables, etc.]

**Tag your Pull Request**
[API Change]
