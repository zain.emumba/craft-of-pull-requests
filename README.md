<p align="center">
  <img src="emumba.svg" alt="Emumba Logo">
</p>

# Craft of Pull Requests

Recording: [Craft of Pull Request](https://drive.google.com/file/d/1HoxRLvBmG94VSmnOJnbzVyK6XizMiDYu/view)

Slides: [Craft of Pull Request](https://drive.google.com/file/d/1pDAatPvaA5iTjHce3PtAwW7t2fHNoxsg/view)

## Introduction

This document outlines the guidelines for creating and submitting pull requests to this repository. Adhering to these guidelines helps maintain code quality and facilitates the review process.

## Setup

### Gitlab

<p align="center">
  <img src="gitlab.svg" alt="Gitlab Logo" width="48">
</p>

You will need to follow few steps to enable templates for PRs in Gitlab

1. Create `.gitlab` directory at the root of your project. If this directory doesn't exist, create it.
2. Create `merge_request_templates` directory within the `.gitlab` directory
3. Paste your template withing the `merge_request_templates` directory
   - bug_fix_template.md
   - feature_request_template.md
4. Now whenever you create a new PR Gitlab has a dropdown for template in PR creation mode
5. Select whatever templates you want to use ✨

PS. the sample is provided within the repo as well under the folder named `.gitlab`

<hr/>
<br/>

### Github

<p align="center">
  <img src="github.svg" alt="Gitlab Logo" width="32">
</p>

You will need to follow few steps to enable templates for PRs in Github

**NOTE:** Github multiple templates format isn't as intuitive as Gitlab's, in order to implement that the steps to in corporate multiple templats is listed below. But I would recommend using single template as it will be filled by default when creating a PR.

#### Create a PR template in your repository:

1. Navigate to your repository on GitHub.
2. Create a new file in the `.github/PULL_REQUEST_TEMPLATE` directory. If this directory doesn't exist, create it.
3. Name the file `pull_request_template.md`.
4. Add your PR template content to this file. Save and commit the file.

#### Using multiple PR templates:

1. If you need different templates for different situations, create each template in the `.github/PULL_REQUEST_TEMPLATE` directory.
2. Name each file distinctly (e.g., `bug_fix_template.md`, `feature_request_template.md`).

#### Link to a specific PR template:

1. When creating a PR, you can specify a template by appending a query parameter to the PR URL: ?template=TEMPLATE_NAME.md.

   - For example: https://github.com/OWNER/REPOSITORY/compare/BRANCH?expand=1&template=bug_fix_template.md
     Automatically use the template:

2. If there's a single pull_request_template.md file in the .github/, docs/, or the repository's root, GitHub will automatically use it for new PRs.

3. If using multiple templates, users need to select the appropriate template as described in step 3.
   Fill out the PR:

When opening a new PR, the template content will appear in the PR description area. Fill it out according to your needs.

## General Guidelines

- [Guidelines](./general-guidelines.md)
